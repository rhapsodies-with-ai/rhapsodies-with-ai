#kanren enables one to express sophisticated relations—in the form of goals

from kanren import Relation, facts, run, var, lall

parent = Relation()

facts(parent, ("Homer", "Bart"), ("Homer", "Lisa"), ("Abe",  "Homer"))

x = var()

print(run(1, x, parent(x, "Bart")))

print(run(2, x, parent("Homer", x)))

#We can express the grandfather relationship as a distinct relation by creating a goal constructor:


def grandparent(x, z):
    y = var()
    return lall(parent(x, y), parent(y, z))

print(run(1, x, grandparent(x, 'Bart')))