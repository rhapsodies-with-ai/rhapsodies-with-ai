import pandas as pd

gss = pd.read_csv('data/gss_bayes.csv', index_col=0)
print(gss.head())

banker = (gss['indus10'] == 6870)
print(banker.head())

print(banker.sum())

print(banker.mean())

def prob(A):
    """Computes the probability of a proposition, A."""
    return A.mean()

print(prob(banker))

female = (gss['sex'] == 2)

print(prob(female))

liberal = (gss['polviews'] <= 3)

print(prob(liberal))

democrat = (gss['partyid'] <= 1)

print(prob(democrat))

print(prob(banker & democrat))

selected = democrat[liberal]

print(prob(selected))

selected = female[banker]

print(prob(selected))

def conditional(proposition, given):
    """Probability of A conditioned on given."""
    return prob(proposition[given])

print(conditional(liberal, given=female))

#Condition and Conjunction

print(conditional(female, given=liberal & democrat))

print(conditional(liberal & female, given=banker))

#Thing No. 1

print(female[banker].mean())

print(conditional(female, given=banker))

print(prob(female & banker) / prob(banker))

#Thing No. 2

print(prob(liberal & democrat))

print(prob(democrat) * conditional(liberal, democrat))

#Thing No. 3

print(conditional(liberal, given=banker))

print(prob(liberal) * conditional(banker, liberal) / prob(banker))

#Thing No. 4

print(prob(banker))

male = (gss['sex'] == 1)

print(prob(male & banker) + prob(female & banker))

print((prob(male) * conditional(banker, given=male) +
prob(female) * conditional(banker, given=female)))

#Thing No. 4 - Generalized

B = gss['polviews']

print(B.value_counts().sort_index())

print(sum(prob(B==i) * conditional(banker, B==i)
    for i in range(1, 8)))









